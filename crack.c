#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password will be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a target hash, crack it. Return the matching
// password.
char *crackHash(char *target, char *dictionary)
{
    // Open the dictionary file
    FILE *dictFile = fopen(dictionary, "r");
    if (!dictFile)
    {
        printf("Can't open %s for reading\n", dictionary);
        exit(1);
    }
    // Loop through the dictionary file, one line
    // at a time.
    char *password = malloc(50);
    while(fgets(password, 50, dictFile) != NULL)
    {
        char *nl = strchr(password, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        // Hash each password. Compare to the target hash.
        // If they match, return the corresponding password.
        char *hash = md5(password, strlen(password));
        if (strcmp(hash, target) == 0)
        {
            return password;
        }
    }            
    // Free up memory?
    fclose(dictFile);
    return NULL;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hashFile dictFile\n", argv[0]);
        exit(1);
    }

    // Open the hash file for reading.
    FILE *hashFile = fopen(argv[1], "r");
    if (!hashFile)
    {
        printf("Can't open %s for reading\n", argv[1]);
        exit(1);
    }

    // For each hash, crack it by passing it to crackHash
    char target[HASH_LEN + 1];

    while(fgets(target, HASH_LEN + 1, hashFile) != NULL)
    {
        char *nl = strchr(target, '\n');
        if (nl != NULL)
        {
            *nl = '\0';
        }
        // Display the hash along with the cracked password:
        //   5d41402abc4b2a76b9719d911017c592 hello
        char *password = crackHash(target, argv[2]);
        printf("%s %s\n", target, password);
        //printf("%s\n", target);
    }
    
    
    // Close the hash file
    fclose(hashFile);
    // Free up any malloc'd memory?
    return 0;
}
